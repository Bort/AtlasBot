﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibary.Models
{
    public enum Role
    {
        Top,
        Jungle,
        Mid,
        ADC,
        Support,
        Fill
    }
}
